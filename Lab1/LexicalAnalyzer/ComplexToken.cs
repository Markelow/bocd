﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LexicalAnalyzer
{
    class ComplexToken : BaseToken
    {
        string name;
        
        public string Name
        {
            get { return name; }
        }

        public ComplexToken(int lineNumber, string name) : base(lineNumber)
        {
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            return (this == obj) || (name.Equals((obj as ComplexToken).name));
        }
    }
}
