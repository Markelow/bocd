﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LexicalAnalyzer
{
    class SimpleToken : BaseToken
    {
        char symbol;

        public char Symbol
        {
            get { return symbol; }
        }

        public SimpleToken(int lineNumber, char symbol)
            : base(lineNumber)
        {
            this.symbol = symbol;
        }
    }
}
