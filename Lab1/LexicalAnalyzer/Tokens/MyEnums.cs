﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LexicalAnalyzer.Tokens
{
    //Перечисление имен токена
    public enum TokenName
    {
        Constant, Identifier, Delimiter, Assignment, KeyWord,
        OpenningBracket, ClosingBracket, Unary, AddSub, MultDiv, Error, None, EndText
    }

    //Перечисление значений атрибута токена
    public enum TokenAttributeValue
    {
        Begin, End, Int, Plus, Minus, Multiplication, Division,
        Semicolon, Comma
    }
}
