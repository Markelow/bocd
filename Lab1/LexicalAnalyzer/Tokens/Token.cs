﻿namespace LexicalAnalyzer.Tokens
{
    //Класс лексем, содеражащий только номер строки, в которой находится лексемаы
    public class Token
    {
        TokenName name;
        TokenAttribute attribute;

        public TokenName Name
        {
            get { return name; }
        }

        public TokenAttribute Attribute
        {
            get { return attribute; }
        }

        public Token(TokenName name, TokenAttribute attribute)
        {
            this.name = name;
            this.attribute = attribute;
        }

        public override string ToString()
        {
            return name.ToString() + "\t|\t" + attribute.ToString();
        }
    }
}
