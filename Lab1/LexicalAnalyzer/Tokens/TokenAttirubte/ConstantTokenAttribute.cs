﻿using System;

namespace LexicalAnalyzer
{
    /// <summary>
    /// Атрибут токена константы
    /// </summary>
    public class ConstantTokenAttribute : TokenAttribute
    {
        private int ToBase = 16;
 
        int value;
        
        /// <summary>
        /// Возвращает значение коснтанты
        /// </summary>
        public int Value
        {
            get { return value; }
        }

        public ConstantTokenAttribute(int lineNumber, int value) 
            : base(lineNumber)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return Convert.ToString(value, ToBase);
        }
    }
}
