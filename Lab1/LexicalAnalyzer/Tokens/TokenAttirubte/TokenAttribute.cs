﻿namespace LexicalAnalyzer
{
    /// <summary>
    /// Атрибут для токенов, которым не нужно хранить значение
    /// </summary>
    public class TokenAttribute
    {
        protected int lineNumber;

        public int LineNumber
        {
            get { return lineNumber; }
        }

        public TokenAttribute(int lineNumber)
        {
            this.lineNumber = lineNumber;
        }

        public override string ToString()
        {
            return "-";
        }
    }
}
