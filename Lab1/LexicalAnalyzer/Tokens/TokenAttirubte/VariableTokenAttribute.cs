﻿namespace LexicalAnalyzer
{
    /// <summary>
    /// Атрибут токена идентифактора
    /// </summary>
    public class IdentifierTokenAttribute : TokenAttribute
    {
        int value;
        
        /// <summary>
        /// Возвращает номер строки таблицы символов, в которой хранится данный идентификатор
        /// </summary>
        public int Value
        {
            get { return value; }
        }

        public IdentifierTokenAttribute(int lineNumber, int value) 
            : base(lineNumber)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
