﻿namespace LexicalAnalyzer
{
    /// <summary>
    /// Атрибут токена идентифактора или константы
    /// </summary>
    class ReferenceTokenAttribute : TokenAttribute
    {
        int value;
        
        /// <summary>
        /// Возвращает номер строки таблицы символов или констант, в которой хранится данный идентификатор или константа
        /// </summary>
        public int Value
        {
            get { return value; }
        }

        public ReferenceTokenAttribute(int lineNumber, int value) 
            : base(lineNumber)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
