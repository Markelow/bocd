﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LexicalAnalyzer.Tokens;

namespace LexicalAnalyzer
{
    /// <summary>
    /// Атрибут токена ключевых слов, операторов, разделителей, которым нужно хранить значение
    /// </summary>
    public class ElementTokenAttribute : TokenAttribute
    {
        TokenAttributeValue value;

        public TokenAttributeValue Value
        {
            get { return value; }
        }

        public ElementTokenAttribute(int lineNumber, TokenAttributeValue value)
            : base(lineNumber)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
