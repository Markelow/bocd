﻿namespace LexicalAnalyzer
{
    /// <summary>
    /// Атрибут токена ошибки
    /// </summary>
    public class ErrorTokenAttribute : TokenAttribute
    {
        string value;

        /// <summary>
        /// Текст программы, который обработался как ошибка
        /// </summary>
        public string Value
        {
            get { return value; }
        }

        public ErrorTokenAttribute(int lineNumber, string value)
            : base(lineNumber)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value + " in " + lineNumber.ToString() + " line";
        }
    }
}
