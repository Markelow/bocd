﻿using System.Linq;
using System.Collections.Generic;
using LexicalAnalyzer.Tokens;

namespace LexicalAnalyzer
{
    public class LexicalAnalyzer
    {
        const string BeginWord = "Begin";
        const string EndWord = "End";
        const string IntWord = "Int";
        readonly string[] KeyWords = { BeginWord, EndWord, IntWord };

        Dictionary<int, string> symbolTable;

        public Dictionary<int, string> SymbolTable
        {
            get { return symbolTable; }
        }
        
        public LexicalAnalyzer()
        {
            symbolTable = new Dictionary<int, string>();
        }
        
        /// <summary>
        /// Проводит лексический анализ текста программы, передаваемого через аргументы
        /// </summary>
        /// <param name="text">Тест программы</param>
        public List<Token> Analyse(string text)
        {
            List<Token> result = new List<Token>();
            TokenName prevToken = TokenName.None;
            int lineNumber = 1;
            for (int i = 0; i < text.Length; ++i)
            {
                if (char.IsDigit(text[i]))
                {
                    string number = text[i].ToString();
                    while ((i + 1 < text.Length) && char.IsDigit(text[i+1]))
                    {
                        ++i;
                        number += text[i].ToString();
                    }
                    int attributeValue = int.Parse(number);
                    prevToken = TokenName.Constant;
                    result.Add(new Token(TokenName.Constant, new ConstantTokenAttribute(lineNumber, attributeValue)));
                    continue;
                }
                if (char.IsLetter(text[i]))
                {
                    string value = text[i].ToString();
                    while ((i + 1 < text.Length) && char.IsLetter(text[i+1]))
                    {
                        ++i;
                        value += text[i].ToString();
                    }
                    if (KeyWords.Contains(value))
                    {
                        ElementTokenAttribute tokenAttribute = null;
                        switch (value)
                        {
                            case BeginWord:
                                tokenAttribute = new ElementTokenAttribute(lineNumber, TokenAttributeValue.Begin);
                                break;
                            case EndWord:
                                tokenAttribute = new ElementTokenAttribute(lineNumber, TokenAttributeValue.End);
                                break;
                            case IntWord:
                                tokenAttribute = new ElementTokenAttribute(lineNumber, TokenAttributeValue.Int);
                                break;
                        }
                        prevToken = TokenName.KeyWord;
                        result.Add(new Token(TokenName.KeyWord, tokenAttribute));
                    }
                    else
                    {
                        bool isIdentifier = true;
                        for (int j = 0; i < value.Length; ++i)
                        {
                            if (char.IsUpper(value[j])) { isIdentifier = false; }
                        }
                        if (isIdentifier)
                        {
                            int attributeValue;
                            if (!symbolTable.Values.Contains(value))
                            {
                                attributeValue = symbolTable.Count;
                                symbolTable.Add(attributeValue, value);
                            }
                            else
                            {
                                attributeValue = symbolTable.FirstOrDefault(x => x.Value == value).Key;
                            }
                            prevToken = TokenName.Identifier;
                            result.Add(new Token(TokenName.Identifier, new IdentifierTokenAttribute(lineNumber, attributeValue)));
                        }
                        else
                        {
                            prevToken = TokenName.Error;
                            result.Add(new Token(TokenName.Error, new ErrorTokenAttribute( lineNumber, value)));
                        }
                    }
                    continue;
                }
                switch (text[i])
                {
                    case '\n': 
                        lineNumber++;
                        break;
                    case '\t':
                    case '\r':
                    case ' ':
                        break;
                    case ',':
                        prevToken = TokenName.Delimiter;
                        result.Add(new Token(TokenName.Delimiter, new ElementTokenAttribute(lineNumber, TokenAttributeValue.Comma)));
                        break;
                    case ';':
                        prevToken = TokenName.Delimiter;
                        result.Add(new Token(TokenName.Delimiter, new ElementTokenAttribute(lineNumber, TokenAttributeValue.Semicolon)));
                        break;
                    case ':':
                        if ((i+1 < text.Length) && ('=' == text[i+1]))
                        {
                            prevToken = TokenName.Assignment;
                            result.Add(new Token(TokenName.Assignment, new TokenAttribute(lineNumber)));
                            ++i;
                        }
                        else
                        {
                            prevToken = TokenName.Error;
                            result.Add(new Token(TokenName.Error, new ErrorTokenAttribute(lineNumber, ":")));
                        }
                        break;
                    case '(':
                        prevToken = TokenName.OpenningBracket;
                        result.Add(new Token(TokenName.OpenningBracket, new TokenAttribute(lineNumber)));
                        break;
                    case ')':
                        prevToken = TokenName.ClosingBracket;
                        result.Add(new Token(TokenName.ClosingBracket, new TokenAttribute(lineNumber)));
                        break;
                    case '+':
                        prevToken = TokenName.AddSub;
                        result.Add(new Token(TokenName.AddSub, new ElementTokenAttribute(lineNumber, TokenAttributeValue.Plus)));
                        break;
                    case '-':
                        switch (prevToken)
                        {
                            case TokenName.Assignment:
                            case TokenName.OpenningBracket:
                                prevToken = TokenName.Unary;
                                result.Add(new Token(TokenName.Unary, new TokenAttribute(lineNumber)));
                                break;
                            default:
                                prevToken = TokenName.Error;
                                result.Add(new Token(TokenName.AddSub, new ElementTokenAttribute(lineNumber, TokenAttributeValue.Minus)));
                                break;
                        }
                        break;
                    case '*':
                        prevToken = TokenName.MultDiv;
                        result.Add(new Token(TokenName.MultDiv, new ElementTokenAttribute(lineNumber, TokenAttributeValue.Multiplication)));
                        break;
                    case '/': 
                        //проверка, является ли / началом комментария и пропуск текста, если это комментарий
                        if ((i + 1 < text.Length) && '*' == text[i + 1])
                        {
                            int currentLine = lineNumber;
                            int currentPosit = i;
                            while (('*' != text[i]) || ('/' != text[i + 1]))
                            {
                                if ('\n' == text[i]) { lineNumber++; }
                                //проверка закрытие комментария
                                if (i + 1 < text.Length) { ++i; }
                                else
                                {
                                    prevToken = TokenName.Error;
                                    result.Add(new Token(TokenName.Error, new ErrorTokenAttribute(currentLine, text[currentPosit].ToString())));
                                    break;
                                }
                            }
                        }
                        else
                        {
                            prevToken = TokenName.MultDiv;
                            result.Add(new Token(TokenName.MultDiv, new ElementTokenAttribute(lineNumber, TokenAttributeValue.Division)));
                        }
                        break;
                    default:
                        prevToken = TokenName.Error;
                        result.Add(new Token(TokenName.Error, new ErrorTokenAttribute(lineNumber, text[i].ToString())));
                        break;
                }
            }
            result.Add(new Token(TokenName.EndText, new TokenAttribute(lineNumber)));
            return result;
        }             
    }
}
