﻿using System;
using System.IO;
using System.Collections.Generic;
using LexicalAnalyzer.Tokens;

namespace LexicalAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (0 == args.Length) { throw new Exception("Не указан путь к файлу с текстом программы."); }
                string textProgram = ReadTextProgram(args[0]);
                LexicalAnalyzer lexer = new LexicalAnalyzer();
                Console.WriteLine("Analyze {0} started...", args[0]);
                List<Token> lexems = lexer.Analyse(ReadTextProgram(args[0]));
                Console.WriteLine("Analyze {0} completed.", args[0]);
                SaveLexemsToFile(lexems, "lexems_" + args[0]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Считывает текст программы из указанного файла
        /// </summary>
        /// <param name="path">Путь к файлу с текстом программы</param>
        /// <returns>Текст считанной программы</returns>
        private static string ReadTextProgram(string path)
        {
            using (TextReader textReader = new StreamReader(path))
            {
                return textReader.ReadToEnd();
            }
        }

        /// <summary>
        /// Сохраняет результаты лексического анализа в указанный файл
        /// </summary>
        /// <param name="lexems">Список лексем</param>
        /// <param name="path">Путь к файл, куда следует сохранить результаты</param>
        private static void SaveLexemsToFile(List<Token> lexems, string path)
        {
            using (TextWriter textWriter = new StreamWriter(path, false))
            {
                textWriter.WriteLine("Lexical analyze results:\nTokenName\t|\tAttributeValue\n");
                foreach (Token lexem in lexems)
                {
                    textWriter.WriteLine(lexem);
                }
            }
        }
    }
}
