﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LexicalAnalyzer
{
    abstract class BaseToken
    {
        protected int lineNumber;
        public int LineNumber
        {
            get { return lineNumber; }
        }

        public BaseToken(int lineNumber)
        {
            this.lineNumber = lineNumber;
        }
    }
}
