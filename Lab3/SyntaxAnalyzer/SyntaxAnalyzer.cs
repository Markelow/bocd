﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LexicalAnalyzer.Tokens;
using LexicalAnalyzer;

namespace SyntaxAnalyzer
{
    //Синтаксический анализатор
    public class SyntaxAnalyzer
    {
        Grammar grammar;
        const int startLexem = 1;

        public SyntaxAnalyzer()
        {
            grammar = new Grammar();
        }

        /// <summary>
        /// Выполняет синтаксический анализ
        /// </summary>
        /// <param name="tokens">Список токенов от лексического анализатора</param>
        /// <returns>Результат лексического анализа со списком использованных правил и указанием ошибок, если они были</returns>
        public SyntaxAnalysisResult Analyze(List<LexicalAnalyzer.Tokens.Token> tokens)
        {
            bool haveError, isCompleted, CanSetVariables;
            haveError = false;
            isCompleted = false;
            CanSetVariables = false;

            int inputLexem;
            int currentLine;
            currentLine = 0;

            Queue<int> usedRules;
            usedRules = new Queue<int>();

            List<int> variables;
            variables = new List<int>();

            Stack<int> stack = new Stack<int>();
            stack.Push(Grammar.EndTextLexem);
            stack.Push(startLexem);

            foreach (Token token in tokens)
            {
                currentLine = token.Attribute.LineNumber;
                inputLexem = grammar.GetLexemNumber(token);
                while (!isCompleted && !haveError)
                {
                    int stackLexem = stack.Peek();
                    if (Grammar.EpsilonLexem == stackLexem)
                    {
                        stack.Pop();
                        stackLexem = stack.Peek();
                    }
                    if (Grammar.IdentifierLexem == inputLexem)
                    {
                        if (CanSetVariables) { variables.Add((token.Attribute as IdentifierTokenAttribute).Value); }
                        else
                        {
                            if (!variables.Contains((token.Attribute as IdentifierTokenAttribute).Value))
                            {
                                haveError = true;
                                break;
                            }
                        }
                    }
                    if (stackLexem == inputLexem)
                    {
                        if (Grammar.EndTextLexem == stackLexem) { isCompleted = true; }
                        else { stack.Pop(); }
                        break;
                    }
                    if (Grammar.EndTextLexem == stackLexem)
                    {
                        haveError = true;
                        break;
                    }
                    if (101 <= stackLexem)
                    {
                        haveError = true;
                        break;
                    }
                    if (0 >= grammar[stackLexem, inputLexem])
                    {
                        haveError = true;
                        break;
                    }
                    stack.Pop();
                    usedRules.Enqueue(grammar[stackLexem, inputLexem ]);
                    if (Grammar.IntLexem == inputLexem) { CanSetVariables = true; }
                    if (Grammar.BeginLexem == inputLexem) { CanSetVariables = false; }
                    foreach (int newLexem in grammar.Rules[grammar[stackLexem, inputLexem]])
                    {
                        stack.Push(newLexem);
                    }
                }
            }
            return new SyntaxAnalysisResult(usedRules, haveError, currentLine);
        }

    }
}
