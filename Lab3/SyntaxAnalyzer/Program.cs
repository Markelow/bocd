﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LexicalAnalyzer;
using System.IO;

namespace SyntaxAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (0 == args.Length) { throw new Exception("Не указан путь к файлу с текстом программы."); }
                string textProgram = ReadTextProgram(args[0]);
                //lexical analyze
                LexicalAnalyzer.LexicalAnalyzer lexer = new LexicalAnalyzer.LexicalAnalyzer();
                Console.WriteLine("Lexical analyze {0} started...", args[0]);
                List<LexicalAnalyzer.Tokens.Token> lexems = lexer.Analyse(ReadTextProgram(args[0]));
                Console.WriteLine("Lexical analyze {0} completed.", args[0]);
                //syntax analyze
                SyntaxAnalyzer syntaxer = new SyntaxAnalyzer();
                Console.WriteLine("Syntax analyze {0} started...", args[0]);
                SyntaxAnalysisResult results = syntaxer.Analyze(lexems);
                Console.WriteLine("Syntax analyze {0} completed.", args[0]);
                SaveResultsToFile(results, Path.GetDirectoryName(args[0]) + "syntax_" + Path.GetFileName(args[0]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// Считывает текст программы из указанного файла
        /// </summary>
        /// <param name="path">Путь к файлу с текстом программы</param>
        /// <returns>Текст считанной программы</returns>
        private static string ReadTextProgram(string path)
        {
            using (TextReader textReader = new StreamReader(path))
            {
                return textReader.ReadToEnd();
            }
        }

        /// <summary>
        /// Сохраняет результаты синтаксического анализа в указанный файл
        /// </summary>
        /// <param name="result">Результаты синтаксического анализа</param>
        /// <param name="path">Путь к файл, куда следует сохранить результаты</param>
        private static void SaveResultsToFile(SyntaxAnalysisResult result, string path)
        {
            using (TextWriter textWriter = new StreamWriter(path, false))
            {
                textWriter.WriteLine("Syntax analyze results:");
                int count = result.UsedRules.Count;
                int usedRuleId = 0;
                GrammarRules rules = new GrammarRules();
                for (int i = 0; i < count; ++i )
                {
                    usedRuleId = result.UsedRules.Dequeue();
                    textWriter.WriteLine("Rule " + usedRuleId.ToString() + " : " + rules.GetRuleDisciription(usedRuleId));
                }
                if (result.HaveError) 
                {
                    string errorMessage = "Syntax Error in line " + result.ErrorLineNumber.ToString() + ". ";
                    errorMessage += "Expected " + rules.GetRuleDisciription(usedRuleId);
                    textWriter.WriteLine(errorMessage); 
                }
            }
        }
    }
}
