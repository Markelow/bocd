﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LexicalAnalyzer.Tokens;
using LexicalAnalyzer;

namespace SyntaxAnalyzer
{
    //Класс грамматики, содержамий набор правил, таблицу переходов и константные значения терминалов
    public class Grammar
    {
        //Константные значения терминалов
        public const int BeginLexem = 101;
        public const int EndLexem = 102;
        public const int IntLexem = 103;
        public const int IdentifierLexem = 104;
        public const int Semicolomn = 105;
        public const int CommaLexem = 106;
        public const int AssignmentLexem = 107;

        public const int ConstLexem = 108;
        public const int UnaryLexem = 109;
        public const int BinaryOpLexem = 110;
        public const int EpsilonLexem = 111;
        public const int OpenningBracketLexem = 112;
        public const int ClosingBracketLexem = 113;

        public const int EndTextLexem = 10000;
        public const int ErrorLexem = -2;

        
        int[,] predicateTable;
        //Таблица переходов
        public int[,] PredicateTable
        {
            get { return predicateTable; }
            private set { predicateTable = value; }
        }

        GrammarRules rules;
        //Правила в закодированном виде
        public GrammarRules Rules
        {
            get { return rules; }
            set { rules = value; }
        }

        public Grammar()
        {
            rules = new GrammarRules();
            predicateTable = new int[14, 13] {
                {0,	 0,	 1,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0},
                {0,	 0,	 3,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0},
                {2,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0},
                {0,	 0,	 0,	 7,  0,	 0,	 0,	 0,	 0,	 0,	 0,  0,	 0},
                {0,	 0,	 0,	 4,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,  0},
                {0,	 0,	 0,	 0,	 5,	 6,	 0,	 0,	 0,	 0,	 0,	 0,	 0},
                {19, 0,	 18, 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0},
                {0,	 0,	 0,	 8,	 0,	 0,  0,	 0,	 0,	 0,	 0,	 0,	 0},
                {0,	 21, 0,	20,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0,	 0},
                {0,	 0,	 0,	12,	 0,	 0,	 0,	12,	11,	 0,	 0,	12,	 0},
                {0,	 0,	 0,	 9,  0,	 0,	 0, 10,	 0,	 0,	 0,	 0,	 0},
                {0,	 0,	 0,	13,	 0,	 0,	 0, 13,	 0,	 0,	 0,	13,  0},
                {0,	 0,	 0,	17,	 0,	 0,	 0,	17,	 0,	 0,	 0, 16,	 0},
                {0,	 0,	 0,	 0,	15,	 0,	 0,	 0,	 0,	14,	 0,	 0,	15},
            };
        }

        //Возвращает константное значение для терминала по его токену
        public int GetLexemNumber(Token token)
        {
            switch (token.Name)
            {
                case TokenName.Identifier:
                    return Grammar.IdentifierLexem;
                case TokenName.Constant:
                    return Grammar.ConstLexem;
                case TokenName.AddSub:
                case TokenName.MultDiv:
                    return Grammar.BinaryOpLexem;
                case TokenName.Assignment:
                    return Grammar.AssignmentLexem;
                case TokenName.ClosingBracket:
                    return Grammar.ClosingBracketLexem;
                case TokenName.Delimiter:
                    int lexem = (TokenAttributeValue.Comma == (token.Attribute as ElementTokenAttribute).Value) ? 106 : 105;
                    return lexem;
                case TokenName.EndText:
                    return Grammar.EndTextLexem;
                case TokenName.KeyWord:
                    ElementTokenAttribute tokenAttribute = token.Attribute as ElementTokenAttribute;
                    switch (tokenAttribute.Value)
                    {
                        case TokenAttributeValue.Begin:
                            return Grammar.BeginLexem;
                        case TokenAttributeValue.Int:
                            return Grammar.IntLexem;
                        case TokenAttributeValue.End:
                            return Grammar.EndLexem;
                        default:
                            return Grammar.ErrorLexem;
                    }
                case TokenName.OpenningBracket:
                    return Grammar.OpenningBracketLexem;
                case TokenName.Unary:
                    return Grammar.UnaryLexem;
                default:
                    return Grammar.ErrorLexem;
            }
        }

        //Возвращает номер слудеющего правила
        public int this[int i, int j]
        {
            get { return predicateTable[i-1, j-101]; }
        }
    }
}
