﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxAnalyzer
{
    //Класс результат синтаксического анализа
    public class SyntaxAnalysisResult
    {
        bool haveError;
        //Признак ошибки в анализе
        public bool HaveError
        {
            get { return haveError; }
            private set { haveError = value; }
        }

        int errorLineNumber;
        //Номер строки, в которой была обнаружена ошибка
        public int ErrorLineNumber
        {
            get { return errorLineNumber; }
            private set { errorLineNumber = value; }
        }

        Queue<int> usedRules;
        //Использованные правила при анализе программы
        public Queue<int> UsedRules
        {
            get { return usedRules; }
            private set { usedRules = value; }
        }

        public SyntaxAnalysisResult(Queue<int> usedRules, bool haveError, int errorLineNumber = -1)
        {
            HaveError = haveError;
            UsedRules = usedRules;
            ErrorLineNumber = errorLineNumber;
        }
    }
}
