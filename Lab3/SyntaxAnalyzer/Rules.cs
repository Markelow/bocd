﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxAnalyzer
{
    //Класс для хранения правил в закодиорванном виде, а также словевсных описаний терминалов и нетерминалов
    public class GrammarRules
    {
        //Словесное описание терминалов и нетерминалов
        Dictionary<int, string> discription;
        public Dictionary<int, string> Discription
        {
            get { return discription; }
        }

        int[][] rules;
        public int[][] Rules
        {
            get { return rules; }
            private set { rules = value; }
        }

        public GrammarRules()
        {
            rules = new int[21][];

            rules[0] = new int[] { 3, 2 };
            rules[1] = new int[] { 102, 4, 101 };
            rules[2] = new int[] { 5, 103 };
            rules[3] = new int[] { 6, 104 };
            rules[4] = new int[] { 7, 105 };
            rules[5] = new int[] { 5, 106 };
            rules[6] = new int[] { 9, 8 };
            rules[7] = new int[] { 105, 10, 107, 104 };
            rules[8] = new int[] { 104 };
            rules[9] = new int[] { 108 };
            rules[10] = new int[] { 12, 109 };
            rules[11] = new int[] { 12 };
            rules[12] = new int[] { 14, 13 };
            rules[13] = new int[] { 14, 13, 110 };
            rules[14] = new int[] { 111 };
            rules[15] = new int[] { 113, 10, 112 };
            rules[16] = new int[] { 11 };
            rules[17] = new int[] { 2 };
            rules[18] = new int[] { 111 };
            rules[19] = new int[] { 4 };
            rules[20] = new int[] { 111 };  

            discription = new Dictionary<int, string>();
            discription.Add(1, "<Программа>");
            discription.Add(2, "<Объявление переменных>");
            discription.Add(3, "<Описание вычислений>");
            discription.Add(4, "<Список присваиваний>");
            discription.Add(5, "<Список переменных>");
            discription.Add(6, "<Список переменных_1>");
            discription.Add(7,  "<Объявление переменных_1>");
            discription.Add(8, "<Присваивание>");
            discription.Add(9, "<Список присваиваний_1>");
            discription.Add(10, "<Выражение>");
            discription.Add(11, "<Операнд>");
            discription.Add(12, "<Подвыражение>");
            discription.Add(13, "<Подвыражение_1>");
            discription.Add(14, "<Подвыражение_2>");

            discription.Add(101, "\"Begin\"");
            discription.Add(102, "\"End\"");
            discription.Add(103, "\"Int\"");
            discription.Add(104, "\"Идент\"");
            discription.Add(105, "\";\"");
            discription.Add(106, "\",\"");
            discription.Add(107, "\":=\"");
            discription.Add(108, "\"Const\"");
            discription.Add(109, "\"Ун.оп.\"");
            discription.Add(110, "\"Бин.оп.\"");
            discription.Add(111, "\"\"");
            discription.Add(112, "\"(\"");
            discription.Add(113, "\")\"");           
        }

        //Возвращает выбранное правило в закодированном виде
        public int[] this[int i]
        {
            get { return rules[i-1]; }
        }

        public string GetRuleDisciription(int ruleId)
        {
            string ruleDiscription = "";
            for (int i = rules[ruleId-1].Length-1; i>=0; --i)
            {
                ruleDiscription += discription[rules[ruleId-1][i]];
            }
            return ruleDiscription;
        }
    }
}
